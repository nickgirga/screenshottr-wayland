# Screenshottr
Easily take screenshots on wayland.<br />
Intended for use on mobile devices.

# Dependencies
 - [Python 3](https://www.python.org/downloads/)
 - [PyGObject](https://pypi.org/project/PyGObject/)
 - [GTK 3](https://www.gtk.org/)
 - [grim](https://github.com/emersion/grim)

# Obtaining
### Arch Linux
Finished pkg.tar.xz packages created for Arch Linux using the makepkg system can be found under the [releases](https://gitlab.com/nickgirga/screenshottr-wayland/-/releases) page. Simply download one using a web browser. If you wish to create this package yourself, downloading just the [PKGBUILD](https://gitlab.com/nickgirga/screenshottr-wayland/-/blob/master/PKGBUILD/PKGBUILD) in the [PKGBUILD directory](https://gitlab.com/nickgirga/screenshottr-wayland/-/tree/master/PKGBUILD) of the repository should suffice. After you've acquired a pkg.tar.xz or PKGBUILD proceed to [Installation](#installation).

### Else
You can obtain Screenshottr by running `git clone https://gitlab.com/nickgirga/screenshottr-wayland.git` to clone the repository using git. You can also download a compressed archive ([zip](https://gitlab.com/nickgirga/screenshottr-wayland/-/archive/master/screenshottr-wayland-master.zip), [tar.gz](https://gitlab.com/nickgirga/screenshottr-wayland/-/archive/master/screenshottr-wayland-master.tar.gz), [tar.bz2](https://gitlab.com/nickgirga/screenshottr-wayland/-/archive/master/screenshottr-wayland-master.tar.bz2), [tar](https://gitlab.com/nickgirga/screenshottr-wayland/-/archive/master/screenshottr-wayland-master.tar)) using the download button in the [project overview](https://gitlab.com/nickgirga/screenshottr-wayland) or from one of the releases on the [releases](https://gitlab.com/nickgirga/screenshottr-wayland/-/releases) page. If you downloaded a compressed archive, decompress it using the respective tool (unzip or tar), then head to the [installation](#installation) section.

# Installation
### Arch Linux
You should be able to simply open the pkg.tar.xz file in a GUI package manager and click or tap install (only tested with [pamac-aur](https://aur.archlinux.org/packages/pamac-aur/)). To install the package in the command line, you can run `makepkg -i` or `pacman -U [package_name].pkg.tar.xz` in the same directory as the pkg.tar.xz file—replacing `[package_name]` with the name of your package file. If you wish to rebuild the package before installing it, the `makepkg -fi` command will do it all in one line if run in the same directory as the `PKGBUILD` file.

### Else
If you aren't interested in installing Screenshottr and you wish to use it as a portable application, skip to the [running](#running) section. To install Screenshottr to your system and use the launcher icon, make sure you have all of the [dependencies](#dependencies) installed and run `./install.sh` or `sh ./install.sh` as superuser or with sudo. This will place the resource files in `/usr/share/screenshottr`, create a link to the executable from `/usr/bin/screenshottr`, and create a link to the desktop file from `/usr/share/applications/screenshottr.desktop`. After the installer spits out `Installation complete!`, you should be able to launch the application using the `Screenshottr` launcher icon or by running `screenshottr` in a terminal.

# Removal
### Arch Linux
You can search for `screenshottr-wayland-git` in your GUI package manager to remove it or you can run `pacman -Rns screenshottr-wayland-git`.

### Else
Run `./uninstall.sh` or `sh ./uninstall.sh` as superuser or with sudo. It will remove the resource folder at `/usr/share/screenshottr`, the executable link created at `/usr/bin/screenshottr`, and the desktop file link created at `/usr/share/applications/screenshottr.desktop`. After the uninstaller spits out `Removal complete!`, all of Screenshottr's files aside from user generated ones should be removed. Remember to uninstall any unneeded [dependencies](#dependencies), but make sure they aren't required by other software you have installed.

# Running
### Portable
Make sure you have all of the [dependencies](#dependencies) installed. Simply call `./screenshottr` in the root directory of the repository. If your system does not support interpreting foreign files with third-party interpreters like python using a shebang, you can run `python3 screenshottr`. However this will only work if `python3` is a command that is accessible from anywhere. If your environment does not support this, add the path to your `python3` binary before it, but continue to run the command within the root folder of the repository. This script requires resources such as the glade file, so running it from any other folder will likely result in an error.

### Installed
If you have installed Screenshottr, you should be able to simply launch Screenshottr using the launcher icon titled `Screenshottr`. You can also run Screenshottr from the terminal by running `screenshottr`. Note: the installer script does not install [dependencies](#dependencies).

# Screenshots
![screenshot_0.png](.screenshots/screenshot_0.png)
