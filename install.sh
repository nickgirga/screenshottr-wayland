#!/usr/bin/bash

# quit if not superuser
if [ "$EUID" -ne 0 ]
    then echo "Please run as superuser or with sudo"
    exit
fi

# to-do: install needed dependencies before installing screenshottr

# create resource folder in /usr/share
mkdir -p /usr/share/screenshottr

# copy resources into newly created resource folder
cp -i ./screenshottr /usr/share/screenshottr/screenshottr
cp -i ./screenshottr.glade /usr/share/screenshottr/screenshottr.glade
cp -ir ./res /usr/share/screenshottr/res
cp -i ./screenshottr.desktop /usr/share/applications/screenshottr.desktop

# link screenshottr executable to the /usr/bin folder
if [ ! -f /usr/bin/screenshottr ]; then
    ln -s /usr/share/screenshottr/screenshottr /usr/bin/screenshottr
fi

# link launcher icon to /usr/share/applications
if [ ! -f /usr/share/applications/screenshottr.desktop ]; then
    ln -s /usr/share/screenshottr/screenshottr.desktop /usr/share/applications/screenshottr.desktop
fi

# update launcher icons
update-desktop-database

#report to user that installation is complete
echo Installation complete!
